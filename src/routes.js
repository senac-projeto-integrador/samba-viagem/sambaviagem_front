import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: () => import('./pages/Inicio.vue')
    },
    {
      path: '/',
      name: 'home',
      component: () => import('./pages/Inicio.vue')
    },
    {
        path: '/motorista',
        name: 'Motorista',
        component: () => import('./pages/Motorista.vue')
      },
    {
      path: '/listagem',
      name: 'ListaMotorista',
      component: () => import('./pages/ListagemMotorista.vue')
    },
    {
      path: '/passageiro',
      name: 'Passageiro',
      component: () => import('./pages/Passageiro.vue')
    },
    {
      path: '/viagem_add',
      name: 'Viagem',
      component: () => import('./pages/Viagem_add.vue')
    },
    {
      path: '/viagem_add_passageiros/:id',
      name: 'ViagemPassageiros',
      component: () => import('./pages/Viagem_add_Passageiro.vue')
    },
    {
      path: '/viagem_add_motorista/:id',
      name: 'ViagemMotorista',
      component: () => import('./pages/Viagem_add_Motorista.vue')
    },
    {
      path: '/grafico',
      name: 'Grafico',
      component: () => import('./pages/Grafico.vue')
    },
    {
      path: '/erro',
      name: 'ErroPagina',
      component: () => import('./pages/Erro_Pagina.vue')
    },
    {
      path: '/relatorio_show',
      name: 'Relatorio',
      component: () => import('./pages/Relatorio.vue')
    }
  ]
})

export default router