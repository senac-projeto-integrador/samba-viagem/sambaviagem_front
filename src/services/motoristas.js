import { http } from './config'

export default {

    listar:() => {
        return http.get('motoristas')
        
    },

    salvar:(motorista) => {
        console.log(motorista);
        return http.post('motoristas', motorista)
    },

    atualizar:(motorista) => {
        return http.put('motoristas', motorista)
    }

    // apagar:(motorista) => {
    //     console.log('Enviando pedido de exclusao')
    //     return http.request('motoristas', { data: motorista } )
    // }

}   