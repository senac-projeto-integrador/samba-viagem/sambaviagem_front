import { http } from './config'

export default {

    listar:() => {
        return http.get('passageiros')
    },

    salvar:(passageiro) => {
        console.log(passageiro);
        console.log('Iniciando post do passageiro')
        return http.post('passageiros', passageiro)
    },

    atualizar:(passageiro) => {
        return http.put('passageiros', passageiro)
    }

}